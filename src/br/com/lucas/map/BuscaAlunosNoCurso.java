package br.com.lucas.map;

public class BuscaAlunosNoCurso {

    public static void main(String[] args) {

        Curso javaColecoes = new Curso("Aprendendo as coleções do Java","João da Silva");

        javaColecoes.adicionar(new Aula("Trabalhando com ArrayList", 21));
        javaColecoes.adicionar(new Aula("Trabalhando com ArrayList", 21));
        javaColecoes.adicionar(new Aula("Criando uma Aula", 20));
        javaColecoes.adicionar(new Aula("Modelando com coleções", 24));

        Aluno a1 = new Aluno("José Alves", 34672);
        Aluno a2 = new Aluno("Alfredo Junior", 5617);
        Aluno a3 = new Aluno("Carlos Fernando", 17645);
        //usar no exemplo de alunos com mesma matricula
        Aluno a4 = new Aluno("José Alfredo", 17645);

        javaColecoes.matricular(a1);
        javaColecoes.matricular(a2);
        javaColecoes.matricular(a3);
        javaColecoes.matricular(a4);

        System.out.println("Quem é o aluno com matricula 17645?");
        Aluno aluno = javaColecoes.buscaMatriculado(17645);
        System.out.println("Aluno: " + aluno);

//        System.out.println(javaColecoes.getAlunos());
    }
}
