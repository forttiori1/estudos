package br.com.lucas.queue;

import java.util.LinkedList;
import java.util.Queue;

public class Main {

    public static void main(String[] args) {

        Queue<Integer> fila = new LinkedList<>();

        fila.add(1);
        fila.add(2);
        fila.add(3);
        fila.add(4);

        System.out.println(fila);

        System.out.println(fila.peek());
        System.out.println(fila.remove());

        System.out.println(fila);


    }
}
