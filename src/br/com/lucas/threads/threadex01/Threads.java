package br.com.lucas.threads.threadex01;

public class Threads {

    public static void main(String[] args) {
        new Thread(t1).start();
        new Thread(t2).start();
        new Thread(t3).start();
    }

    private static Runnable t1 = new Runnable() {
        public void run() {
            for (int i = 0; i < 20; i++) {
                System.out.println("Thread 1 em execução: " + i);
            }
        }
    };

    private static Runnable t2 = new Runnable() {
        public void run() {
            for (int j = 0; j < 20; j++) {
                System.out.println("Thread 2 em execução: " + j);
            }
        }
    };

    private static Runnable t3 = new Runnable() {
        public void run() {
            for (int k = 0; k < 20; k++) {
                System.out.println("Thread 3 em execução: " + k);
            }
        }
    };
}