package br.com.lucas.threads.threadex02;

public class TesteGeraRelatorio {
    public static void main(String[] args) {
        BarraDeProgresso barra = new BarraDeProgresso();
        barra.run();

        GeraRelatorio relatorio = new GeraRelatorio();
        relatorio.run();
    }
}
