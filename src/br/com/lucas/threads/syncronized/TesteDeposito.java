package br.com.lucas.threads.syncronized;

import br.com.lucas.threads.threadex03.Cliente;
import br.com.lucas.threads.threadex03.Conta;
import br.com.lucas.threads.threadex03.FazDeposito;

public class TesteDeposito {

    public static void main(String[] args) throws InterruptedException{
        Cliente cli1 = new Cliente(27, "Lucas Rodrigues", "Rua dos Bobos");
        Conta c1 = new Conta(1, 200, 300, cli1); //Saldo 500

        FazDeposito acao = new FazDeposito(c1);
        Thread t1 = new Thread(acao);
        Thread t2 = new Thread(acao);

        t1.start(); //500 + 100 = 600?
        t2.start(); //600 + 100 = 700?

        t1.join();
        t2.join();

        System.out.println(c1);
    }
}
