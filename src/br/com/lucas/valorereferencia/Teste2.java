package br.com.lucas.valorereferencia;

public class Teste2 {
    public static void main(String[] args) {
        Carro v = new Carro();
        v.valor = 50;

        Carro c = new Carro();
        c.ano = 1997;

        dobrarValor(v);
        mudarAnoCarro(c);

        System.out.println(v.valor);
        System.out.println(c.ano);
    }

    public static void dobrarValor(Carro v){
        v.valor = v.valor*2;
    }

    public static void mudarAnoCarro(Carro c){
        c.ano = c.ano + 1;
    }
}

