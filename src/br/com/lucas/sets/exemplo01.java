package br.com.lucas.sets;

import java.util.*;

public class exemplo01 {
    public static void main(String[] args) {

        Collection<String> alunos = new HashSet<>();
        alunos.add("Joao da Silva");
        alunos.add("Jose Lopes");
        alunos.add("Roberto Junior");
        alunos.add("Marcos Carvalho");
        alunos.add("Pedro Abreu");
        alunos.add("Pedro Abreu");
        alunos.add("Marcio Rodrigues");
/*
        for(String aluno : alunos){
            System.out.println(aluno);
        }

        System.out.println(alunos);

        System.out.println(alunos.size());*/
//
//        boolean joseEstaMatriculado = alunos.contains("Jose Lopes");
//        System.out.println(joseEstaMatriculado);
//
//        alunos.remove("Marcos Carvalho");
//        System.out.println(alunos);

        //recebendo esses dados do set em um arraylist

//        System.out.println("*******************************");

        List<String> alunosEmLista = new ArrayList<>(alunos);

        System.out.println(alunosEmLista);


    }
}
