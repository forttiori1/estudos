package br.com.lucas.sets;

/**
 * O LinkedHashSet guarda a ordem em que os dados foram inseridos, porém, assim como o set não é possível acessar através do get.
 *
 *
 * O TreeSet só funciona para classes que são comparable. o TreeSet é muito rápido, mas rápido que as listas, não tão rápido quanto hashset
 * O TreeSet guarda os dados numa tabela de armazenamento chamada arvore rubro negra. Ele guarda os objetos na sua ordem natural (ordem do comparable)
 *  */
