package br.com.lucas.linkedlist;

import br.com.lucas.arraylist.Aula;

import java.util.Collections;
import java.util.List;

public class TestaCurso {

    public static void main(String[] args) {
        Curso javaColecoes = new Curso("Estudando as coleções do Java", "João da Silva");

        javaColecoes.adiciona(new Aula("Trabalhando com ArrayList", 21));
        javaColecoes.adiciona(new Aula("Criando uma aula", 20));
        javaColecoes.adiciona(new Aula("Modelando com coleções", 29));

        System.out.println(javaColecoes.getAulas());

        List<Aula> aulas = javaColecoes.getAulas();
        System.out.println(aulas);

        System.out.println(javaColecoes.getTempoTotal());

        System.out.println(javaColecoes);
    }
}